﻿using System;
namespace AmericaVirtual.Context.Errors
{
    public class NotFoundException : Exception
    {
        public int StatusCode { get; set; }
        
        public NotFoundException()
        {
            StatusCode = 404;
        }

        public NotFoundException(string message) : base(message)
        {
            StatusCode = 404;
        }
    }
}
