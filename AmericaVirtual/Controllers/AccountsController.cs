﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AmericaVirtual.Context;
using AmericaVirtual.Models;
using AuthenticationPlugin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AmericaVirtual.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountsController : Controller
    {
        // GET: api/values
        private MyContext context;
        private IConfiguration _configuration;
        private readonly AuthService _auth;

        public AccountsController(IConfiguration configuration)
        {
            context = new MyContext();
            _configuration = configuration;
            _auth = new AuthService(_configuration);
        }

        [HttpPost]
        [ProducesResponseType(400)]
        [ProducesResponseType(201)]
        public IActionResult Register([FromBody]User user)
        {
            IActionResult result;
            User userFound = context.Users.Where(u => u.Email == user.Email).SingleOrDefault();
            if (userFound == null)
            {
                userFound = context.Users.Where(u => u.UserName == user.UserName).SingleOrDefault();
                if (userFound == null)
                {
                    try
                    {
                        string hashedPassword = SecurePasswordHasherHelper.Hash(user.Password);
                        context.Users.Add(new Models.User(user.UserName, hashedPassword, user.Email));
                        context.SaveChanges();
                        result = StatusCode(StatusCodes.Status201Created);
                    }
                    catch
                    {
                        result = StatusCode(StatusCodes.Status400BadRequest);
                    }
                    
                   
                } else
                {
                    result = BadRequest("El Nombre de Usuario ya esta en uso");
                }
            }  else
            {
                result = BadRequest("El E-Mail ya esta en uso");
            }
            return result;
        }

        [HttpPost]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]

        public IActionResult Login([FromBody]User user)
        {
            IActionResult result;
            User userLogin = context.Users.FirstOrDefault(u => u.Email == user.Email || u.UserName == user.UserName);
            if (userLogin != null)
            {
                if (SecurePasswordHasherHelper.Verify(user.Password, userLogin.Password))
                {
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Email, user.Email),
                        new Claim(ClaimTypes.Email, user.Email),
                    };
                    var token = _auth.GenerateAccessToken(claims);
                   
                    result = new ObjectResult(new
                    {
                        access_token = token.AccessToken,
                        expires_in = token.ExpiresIn,
                        token_type = token.TokenType,
                        creation_Time = token.ValidFrom,
                        expiration_Time = token.ValidTo,
                    });
                    Log log = new Log(userLogin.UserID, "Log In", DateTime.Now);
                    context.Logs.Add(log);
                    context.SaveChanges();
                }
                else
                {
                    result = Unauthorized();
                }
            } else
            {
                result = NotFound();
            }
            return result;
        }
        [Authorize]
        [HttpPost]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]

        public IActionResult Logout() 
        {
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;
            int userId = context.Users.FirstOrDefault(u => u.Email == email).UserID;

            // Por lo que tengo entendido un JWT no puede ser revocado
            
            
            context.Logs.Add(new Log(userId, "Log Out", DateTime.Now));
            context.SaveChanges();

            return Ok(context.Logs.ToList());
           
        }
    }
}
