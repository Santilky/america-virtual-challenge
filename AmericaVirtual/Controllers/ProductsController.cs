﻿using System;
using System.Collections.Generic;
using System.Linq;
using AmericaVirtual.Context;
using AmericaVirtual.Models;
using AmericaVirtual.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AmericaVirtual.Controllers
{

    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private MyContext context;

        public ProductsController()
        {
            context = new MyContext();
        }
        // GET: api/values
        [HttpGet]
        [ProducesResponseType(404)]
        public IActionResult Get([FromQuery] int page, [FromQuery] int pagesize)
        {
            
            IActionResult statusResponse;
            
            List<Product> products = context.Products.ToList();
            if (page > 0 && pagesize > 0)
            {
                try
                {
                    List<Product> productsInPage = Pager.GetArticlesPerPages(page, pagesize, products);
                    statusResponse = Ok(productsInPage);
                } catch
                {
                    statusResponse = NotFound();
                }
                
            } else
            {
                if (products.Count > 0)
                {
                    statusResponse = new OkObjectResult(products);
                }
                else
                {
                    statusResponse = new NotFoundResult();
                }
            }
            
            return statusResponse;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public IActionResult Get(int id)
        {
            Product product = context.Products.FirstOrDefault(p => p.ProductID == id);
            IActionResult statusResponse;
            if (product != null)
            {
                statusResponse = new OkObjectResult(product);
            } else
            {
                statusResponse = new NotFoundResult();
            }
            return statusResponse;
        }

        // POST Requiere estar logeado, es decir tener un JWT activo, podria agregarle que la cuenta tenga un parametro para tener el privilegio enla DB
        [HttpPost]
        [Authorize]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]Product product)
        {
            try
            {
                context.Products.Add(new Product(product.Name, product.Price, product.Description, product.ImageURL));
                context.SaveChanges();
            } catch 
            {
                return ValidationProblem();
            }
            
            return StatusCode(201);
        }

        // PUT api/values/5
    }
}
