﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AmericaVirtual.Context;
using AmericaVirtual.Models;
using AmericaVirtual.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AmericaVirtual.Controllers
{
    [Route("api/[controller]/[Action]")]
    public class OrdersController : Controller
    {
        // GET: api/values
        private MyContext context;
        public OrdersController()
        {
            context = new MyContext();
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        public IActionResult AddProduct([FromBody] OrderDetail orderDetail)
        {
            IActionResult result;
            var product = context.Products.FirstOrDefault(m => m.ProductID == orderDetail.ProductID);
            if (product == null)
            {
                return NotFound();
            }
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;
            var user = context.Users.FirstOrDefault(u => u.Email == email);
            var order = context.Orders.Include(x => x.OrderDetails).FirstOrDefault(o => o.UserID == user.UserID && !o.Finished);
            if (order == null)
            {
                order = new Order(user.UserID, user);
                context.Orders.Add(order);
            }
            //Product doesExists = context.
            var existingOrderDetail = order.OrderDetails.FirstOrDefault(od => od.ProductID == orderDetail.ProductID);
            if (existingOrderDetail == null)
            {
                order.OrderDetails.Add(new OrderDetail(product, orderDetail.ProductID, orderDetail.Amount));
                context.SaveChanges();
                result = Ok(order);
                
            } else
            {
                try
                {
                    existingOrderDetail.addAmount(orderDetail.Amount);
                    context.SaveChanges();
                    result = Ok();
                }
                catch
                {
                    result = BadRequest("La cantidad es inválida");
                }
            }
            return result;
        }
        [HttpPost]
        [Authorize]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(407)]
        [ProducesResponseType(200)]
        public IActionResult Buy()
        {
            IActionResult result;
            string email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;
            var user = context.Users.FirstOrDefault(u => u.Email == email);
            var order = context.Orders.Include(x => x.OrderDetails).FirstOrDefault(o => o.UserID == user.UserID && !o.Finished);

            if (order == null)
            {
                return NoContent();
            }

            decimal total = 100;
            var buyService = new BuyService();
            if (buyService.CommitTransaction(total))
            {
                result = Ok("Total de la compra: " + total);
                order.endOrder();
                context.Logs.Add(new Log(user.UserID, "Compra realizada", DateTime.Now));
                context.SaveChanges();
            } else
            {
                result = StatusCode(407);
            }
            return result;
        }
    }
}
