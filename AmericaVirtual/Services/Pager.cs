﻿using System;
using System.Collections.Generic;
using AmericaVirtual.Models;

namespace AmericaVirtual.Services
{
    public class Pager
    {


        public static List<Product> GetArticlesPerPages(int page, int pageSize, List<Product> products)
        {
            List<Product> results = new List<Product>();
            int totalProducts = products.Count;
            int productsExpected = (page * pageSize);
            int firstProductIndex = productsExpected - pageSize;
            if (firstProductIndex < totalProducts)
            {
                int total = 0;
                if (productsExpected - totalProducts <= 0)
                {
                    total = productsExpected;
                } else
                {
                    total = totalProducts;
                }
                for (int i = firstProductIndex; i < total; i++)
                {
                    results.Add(products[i]);
                }
            } else
            {
                throw new Exception();
            }
            return results;
        }
    }
}
