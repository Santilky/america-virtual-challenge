﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AmericaVirtual.Models
{
    public class Log
    {
        [Key]
        public int LogID { get; set; }
        
        public int UserID { get; set; }
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public Log(int UserId, string Action, DateTime date)
        {
            this.UserID = UserId;
            this.Action = Action;
            this.Date = date;
        }
        public Log()
        {

        }
    }
}
