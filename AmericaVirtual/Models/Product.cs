﻿using System;
using System.ComponentModel.DataAnnotations;



namespace AmericaVirtual.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string ImageURL { get; set; }

        public Product(string name, decimal price, string description, string imageURL)
        {
            this.Name = name;
            this.Price = price;
            this.Description = description;
            this.ImageURL = imageURL;
        }
        public Product()
        {

        }
    }
}
