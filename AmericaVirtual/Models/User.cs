﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AmericaVirtual.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }

        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }

        public User(string _UserName, string _Password, string _Email)
        {
            this.UserName = _UserName;
            this.Password = _Password;
            this.Email = _Email;
        }
        public User()
        {

        }

        public void ValidateUserName(string _UserName)
        {

        }
        public void ValidatePassword(string _Password)
        {

        }
    }
}
