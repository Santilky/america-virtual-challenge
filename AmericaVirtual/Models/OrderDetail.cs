﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AmericaVirtual.Models
{
    public class OrderDetail
    {
        [Key]
        public int OrderDetailID { get; set; }

        public Product Product { get; set; }
        [Required]
        public int ProductID { get; set; }
        [Required]
        public int Amount { get; set; }

        public OrderDetail(Product _product, int _productId,int Amount)
        {
            this.Product = _product;
            this.ProductID = _productId;
            this.Amount = Amount;
        }
        public OrderDetail()
        {

        }

        public void addAmount(int num)
        {
            if (num <= 0)
            {
                throw new Exception();
            }
            this.Amount += num;
        }
    }
}
