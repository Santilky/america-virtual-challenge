﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AmericaVirtual.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        public ICollection<OrderDetail>  OrderDetails{get;set;}
        public bool Finished { get; set; }

        public Order(int userId, User user)
        {
            this.User = user;
            this.UserID = userId;
            this.OrderDetails = new List<OrderDetail>();
            this.Finished = false;
        }
        public Order()
        {
            //this.OrderDetails = new List<OrderDetail>();
        }
        public decimal getTotal()
        {
            decimal total = 0;
            foreach (var detail in OrderDetails)
            {
                total += detail.Amount * detail.Product.Price;
            }
            return total;
        }

        public void endOrder()
        {
            Finished = true;
        }
    }
}
