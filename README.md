# Santiago Linietsky
# Challenge America Virtual - Puesto Back End


# Scripts Necesarios: No son requeridos para el despliegue del servicio

# Tecnologías.Frameworks utilizadas:
 Entity Framework
 Sqlite
 SwuchBuckle(Swagger)

#Consideraciones: 
Al usar JWT para la autenticación, no encontre que se pueda revocar el token, por lo tanto, el metodo de login, ahora seteado por default a 24 horas que devuelve el JSON con el JWT, no hay manera de terminarlo antes.
Posibles soluciones a considerar:
    -Blacklist de JWT o con fechas de creación, aunque podria tener un impacto negativo en el rendimiento.
    -Guardar último JWT, para comparar y revisar si fue el último emitido.
    -Usar otro nuget que implemente esto.
    
Por ésto mismo, acciones como el Log Out, no son posibles de crear al menos desde el back end(en el front simplemente desreferenciar de cualquier memoria el token). 

El controlador de Orders, no se si estaba pedido, al menos lo entendí yo que uno pueda comprar distintos productos, entonces uno agrega productos, la orden se crea parael usuario activo y se agregan Productos, cuando se finaliza el pedido, se Logea como estaá explícito en los Requerimientos
